package com.ruyuan2020.furnishing.tender.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.tender.domain.BiddingDO;

public interface BiddingDAO extends BaseDAO<BiddingDO> {

    Integer countBidding(Long tenderId);

    Integer countBidding(Long tenderId, Long bidderId);

    Long getTenderIdById(Long id);

    void updateSigned(Long id);
}
