package com.ruyuan2020.furnishing.tender.service.state.impl;

import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.TenderState;
import org.springframework.stereotype.Component;

/**
 * 默认招标状态
 */
@Component
public class DefaultTenderState implements TenderState {

    @Override
    public void doTransition(TenderDTO tenderDTO) {

    }

    @Override
    public Boolean canBid(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canSign(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComplete(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComment(TenderDTO tenderDTO) {
        return false;
    }
}
