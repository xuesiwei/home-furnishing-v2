package com.ruyuan2020.furnishing.tender.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruyuan2020.common.domain.JsonResult;
import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.common.util.ResultHelper;
import com.ruyuan2020.furnishing.tender.domain.*;
import com.ruyuan2020.furnishing.tender.service.TenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/tender")
public class TenderController {

    @Autowired
    private TenderService tenderService;

    @GetMapping("/{id}")
    @SentinelResource(value = "api/tender/id",
            blockHandler = "getBlockHandler",
            exceptionsToIgnore = BusinessException.class)
    public JsonResult<?> get(@PathVariable("id") Long id) {
        return ResultHelper.ok(tenderService.get(id).clone(TenderVO.class));
    }

    public String getBlockHandler(Long id, BlockException ex) {
        throw new BusinessException("当前招标信息访问过于频繁");
    }

    /**
     * 提交招标信息
     *
     * @param tenderVO 招标信息
     * @return 结果
     */
    @PostMapping
    public JsonResult<?> create(@RequestBody TenderVO tenderVO) {
        return ResultHelper.ok(tenderService.create(tenderVO.clone(TenderDTO.class)));
    }

    /**
     * 投标人创建投标信息
     *
     * @param biddingVO 投标信息
     * @return 结果
     */
    @PostMapping("/bidding")
    public JsonResult<?> bid(@RequestBody BiddingVO biddingVO) {
        return ResultHelper.ok(tenderService.bid(biddingVO.clone(BiddingDTO.class)));

    }

    /**
     * 招标人签约
     *
     * @param signingVO 签约信息
     * @return 结果
     */
    @PostMapping("/signing")
    public JsonResult<?> sign(@RequestBody SigningVO signingVO) {
        tenderService.sign(signingVO.clone(SigningDTO.class));
        return ResultHelper.ok();
    }

    /**
     * 招标人设置完工
     *
     * @param completionVO 完工信息
     * @return 结果
     */
    @PostMapping("/completion")
    public JsonResult<?> complete(@RequestBody CompletionVO completionVO) {
        tenderService.complete(completionVO.clone(CompletionDTO.class));
        return ResultHelper.ok();
    }
}
