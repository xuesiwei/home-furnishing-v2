package com.ruyuan2020.furnishing.payment.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PaymentDTO extends BaseDomain {

    /**
     * 支付会员id
     */
    private Long memberId;

    /**
     * 支付方式
     */
    private String paymentMethod;

    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 交易流水号
     */
    private String tradeNo;
}
