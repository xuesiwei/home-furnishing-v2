package com.ruyuan2020.furnishing.member.api;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.member.domain.MemberDTO;

public interface MemberApi {

    /**
     * 通知投标已完成
     *
     * @param memberId 会员id
     */
    void informBidCompletedEvent(Long memberId) throws BusinessException;

    /**
     * 通知签约已完成
     *
     * @param memberId 会员id
     */
    void informSignedEvent(Long memberId) throws BusinessException;

    /**
     * 通知评价已完成
     *
     * @param memberId 会员id
     */
    void informCommentEvent(Long memberId) throws BusinessException;

    /**
     * 获取会员信息
     *
     * @param memberId 会员id
     * @return 会员信息
     */
    MemberDTO getMember(Long memberId) throws BusinessException;
}
