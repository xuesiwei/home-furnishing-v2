package com.ruyuan2020.furnishing.trade.api.impl;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.trade.api.TradeApi;
import com.ruyuan2020.furnishing.trade.domain.TradeLogDTO;
import com.ruyuan2020.furnishing.trade.service.TradeService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = TradeApi.class)
public class TradeApiImpl implements TradeApi {

    @Autowired
    private TradeService tradeService;

    @Override
    public TradeLogDTO getTradeLog(String tradeNo) throws BusinessException {
        return tradeService.getTradeLog(tradeNo);
    }

    @Override
    public void informTradeCompletedEvent(String tradeNo) throws BusinessException {
        tradeService.informTradeCompletedEvent(tradeNo);
    }
}
