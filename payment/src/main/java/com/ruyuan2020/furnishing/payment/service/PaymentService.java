package com.ruyuan2020.furnishing.payment.service;


import com.ruyuan2020.furnishing.payment.domain.PaymentDTO;

public interface PaymentService {

    String buildPayUrl(PaymentDTO paymentDTO);

    String notify(String resultCode, String tradeNo);
}
