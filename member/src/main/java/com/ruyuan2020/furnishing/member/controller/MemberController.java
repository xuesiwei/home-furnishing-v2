package com.ruyuan2020.furnishing.member.controller;

import com.ruyuan2020.common.domain.JsonResult;
import com.ruyuan2020.common.util.ResultHelper;
import com.ruyuan2020.furnishing.member.domain.MemberDTO;
import com.ruyuan2020.furnishing.member.domain.MemberVO;
import com.ruyuan2020.furnishing.member.service.MemberService;
import com.scholar.sdk.utils.RollingWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 会员管理模块Controller组件
 */
@RestController
@RequestMapping("api/member")
public class MemberController {

    /**
     * 会员管理模块Service组件
     */
    @Autowired
    private MemberService memberService;

    @Autowired
    private RollingWindow rollingWindow;

    /**
     * 创建会员
     *
     * @param memberVO 会员信息
     * @return 结果
     */
    @PostMapping
    public JsonResult<?> save(@RequestBody MemberVO memberVO) {
        rollingWindow.hit(1);
        return ResultHelper.ok(memberService.save(memberVO.clone(MemberDTO.class)));

    }
}
