package com.ruyuan2020.furnishing.member.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDTO extends BaseDomain {

    private Long id;

    private String username;

    private String password;

    private String type;

    private String mail;

    private String mobile;

    private Integer gender;

    private Long cityId;

    private String realName;
}
