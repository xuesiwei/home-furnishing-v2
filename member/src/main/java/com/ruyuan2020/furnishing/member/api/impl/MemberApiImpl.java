package com.ruyuan2020.furnishing.member.api.impl;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.member.api.MemberApi;
import com.ruyuan2020.furnishing.member.domain.MemberDTO;
import com.ruyuan2020.furnishing.member.service.MemberCompanyService;
import com.ruyuan2020.furnishing.member.service.MemberService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = MemberApi.class)
public class MemberApiImpl implements MemberApi {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberCompanyService memberCompanyService;

    @Override
    public void informBidCompletedEvent(Long memberId) throws BusinessException {
        memberCompanyService.informBidCompletedEvent(memberId);
    }

    @Override
    public void informSignedEvent(Long memberId) throws BusinessException {
        memberCompanyService.informSignedEvent(memberId);
    }

    @Override
    public void informCommentEvent(Long memberId) throws BusinessException {
        memberCompanyService.informCommentEvent(memberId);
    }

    @Override
    public MemberDTO getMember(Long memberId) throws BusinessException {
        return memberService.getMember(memberId);
    }
}
