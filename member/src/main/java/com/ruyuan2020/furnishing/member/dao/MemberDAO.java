package com.ruyuan2020.furnishing.member.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.member.domain.MemberDO;

public interface MemberDAO extends BaseDAO<MemberDO> {

}
