package com.ruyuan2020.furnishing.member.constant;

public class MemberConstants {

    public static final String TYPE_MEMBER = "01";

    public static final String TYPE_COMPANY = "02";

    public static final Integer GENDER_MAN = 1;

    public static final Integer GENDER_WOMAN = 0;
}
