package com.ruyuan2020.furnishing.trade.constant;

public class TradeStatusConstants {

    public static final String STATUS_IN_TRADING = "00";

    public static final String STATUS_FINISHED = "01";
}
