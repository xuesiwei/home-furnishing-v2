package com.ruyuan2020.furnishing.trade.constant;

public class TradeConstants {

    public static final String PAYMENT_TYPE_RECHARGE = "01";

    public static final String PAYMENT_TYPE_TRUST = "02";
}
