package com.ruyuan2020.furnishing.init;

import com.scholar.sdk.init.DataInitializer;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class Main {

    private final static String DATA_INSERT = "data.insert";

    public static void main(String[] args) throws Exception {
            OptionParser parser = new OptionParser();
            OptionSpec<String> insertOpt = parser.accepts(DATA_INSERT).withOptionalArg().ofType(String.class);
            parser.allowsUnrecognizedOptions();
            OptionSet parse = parser.parse(args);
            DataInitializer.initMysqlWithScriptRunner(args, "init.sql");
            if (parse.has(insertOpt) && "yes".equals(parse.valueOf(insertOpt))) {
                DataInitializer.initMysqlWithScriptRunner(args, "memberGroup.sql");
                DataInitializer.initMysqlWithScriptRunner(args, "accountGroup.sql");
                DataInitializer.initMysqlWithScriptRunner(args, "tenderGroup.sql");
            }
    }
}